package com.example.proyecto.controlador;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.proyecto.modelo.info_proyecto;
import com.example.proyecto.service.proyectoService;

@Controller
@RequestMapping
public class controlador {

	@Autowired
	private proyectoService service;

	@GetMapping("/listar")
	public String listar(Model model) {
		List<info_proyecto> proyectos = new ArrayList<info_proyecto>();

		double totalHoras = 0.0;
		int count = 8;
		int prom = 5;
		float hora = 0.0f;
		float semana = 0.0f;
		double totalSemanas = 0.0;
		for (info_proyecto info_proyecto : service.listar()) {
			totalHoras += info_proyecto.getHoras();
			totalSemanas +=info_proyecto.getSemanas();

			
			hora = info_proyecto.getHoras();
			
			hora = hora / count;

			semana = hora / prom;
			info_proyecto.setSemanas(semana);
			semana = 0.0f;
			hora = 0.0f;

			proyectos.add(info_proyecto);

		}

		hora = hora / count;

		semana = hora / prom;

		model.addAttribute("totalHoras", totalHoras);

		model.addAttribute("semana", semana);

		model.addAttribute("totalSemanas", totalSemanas);

		model.addAttribute("proyectos", proyectos);

		return "index";

	}

	@GetMapping("/new")
	public String agregar(Model model) {
		model.addAttribute("proyecto", new info_proyecto());
		return "form";
	}

	@PostMapping("/save")
	public String save(@Validated info_proyecto i, Model model) {
		service.save(i);
		return "redirect:/listar";
	}

	@GetMapping("/editar/{id}")
	public String editar(@PathVariable int id, Model model) {
		java.util.Optional<info_proyecto> proyecto = service.listarId(id);
		model.addAttribute("proyecto", proyecto);
		return "form";
	}

	@GetMapping("/eliminar/{id}")
	public String delete(Model model, @PathVariable int id) {
		service.delete(id);
		return "redirect:/listar";
	}
}
