package com.example.proyecto.controlador;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.proyecto.interfaceService.IcostoService;
import com.example.proyecto.modelo.costo;

@Controller
@RequestMapping
public class costoControlador {
	private static Logger logger = LoggerFactory.getLogger(costoControlador.class);

	@Autowired
	private IcostoService service;

	@GetMapping("/listar2")
	public String listarCosto(Model model) {
		List<costo> costos = new ArrayList<costo>();
		costos = service.listarC();
		logger.debug("costos=[{}]",costos);

		
		int costoP = 0;
		float carga = 1.15f;
		float margen = 1.52f;
		int salario = 2000000;
		float costoTotal = 0;
	    int valorCobrar= 0;
	    int horames = 160;
	    int cantidad = 1;
	    int valorhoraCop = 0;
	    double dolar = 3.700;
	    int valorhoraUsd = 0;
	    int subTotal =0;
	     float porcentaje = 10f;
	     float horaDedicacion = 0f;
	     float Pdedicacion = 8;
	     float duracionmes = 0.33f;
	     float horaesfuerzo = 0;
	     int  costototal = 0;
	     int total = 0;
	     int totalusd = 0;
	     int utilidad = 0;
	     DecimalFormat df= new DecimalFormat("#.000");
	     
	    

		for (costo costo : service.listarC()) {
			costoP = (int) (margen * salario);
			
			valorCobrar = (int) (costoP * carga )  ;
			valorhoraCop = (int) (valorCobrar / horames);
			valorhoraUsd = (int ) (valorhoraCop / dolar);
			horaDedicacion = (int) (porcentaje * Pdedicacion);
			subTotal = (int)(cantidad * valorCobrar);
			horaesfuerzo = (int)(porcentaje * horames * duracionmes );
			costototal = (int)(costoP * cantidad * porcentaje * duracionmes);
			total = (int) (subTotal * cantidad * duracionmes);
			totalusd = (int) (costototal * dolar);
			utilidad = (int) (costototal-total);
			
			
			
			costo.setCosto(costoP);
			costo.setValor_cobrar(valorCobrar);
			costo.setValor_hora_cop(valorhoraCop);
			costo.setValor_hora_usd(valorhoraUsd);
			costo.setSubtotal(subTotal);
			costo.setHora_dedicacion(horaDedicacion);
			costo.setHora_esfuerzo(horaesfuerzo);
			costo.setCosto_total(costototal);
			costo.setTotal(total);
			costo.setTotal_USD(totalusd);
			costo.setUtilidad(utilidad);
			costoTotal += costoP;
			
		}
		
		

		model.addAttribute("costoTotal", costoTotal);
		model.addAttribute("costos", costos);
		model.addAttribute("valor_cobrar", valorCobrar);
		
		
		return "index2";

	}

	@GetMapping("/new2")
	public String agregarr(Model model) {
		model.addAttribute("costo", new costo());
		return "costo";
	}

	@PostMapping("/savee")
	public String save(@Validated costo c, Model model) {
		service.savee(c);
		return "redirect:/listar2";
	}

	@GetMapping("/editarr/{id}")
	public String editar(@PathVariable int id, Model model) {
		java.util.Optional<costo> costo = service.listarId(id);
		model.addAttribute("costo", costo);
		return "costo";
	}

	@GetMapping("/eliminarr/{id}")
	public String delete(Model model, @PathVariable int id) {
		service.deletee(id);
		return "redirect:/listar2";
	}

}
