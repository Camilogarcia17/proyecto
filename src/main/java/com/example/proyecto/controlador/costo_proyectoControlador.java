package com.example.proyecto.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.proyecto.interfaceService.Icosto_proyectoService;

import com.example.proyecto.modelo.costo_proyecto;

@Controller
@RequestMapping
public class costo_proyectoControlador {

	@Autowired
	private Icosto_proyectoService service;

	
	
	public String listarcostoProyecto(Model model) {
		List<costo_proyecto> costosproyec = service.listarCosto_proyecto();
		model.addAttribute("costosproyec", costosproyec);
		return "null";

	}

}
