package com.example.proyecto.controlador;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.proyecto.interfaceService.IreferenciaService;
import com.example.proyecto.modelo.referencia;

@Controller
@RequestMapping
public class referenciaControlador {

	private IreferenciaService service;
	
	public String listarReferencia(Model model) {
		List<referencia>referencias=service.listarReferencia();
		model.addAttribute("referencias",referencias);
		return null;
	}
}
