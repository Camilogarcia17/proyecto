package com.example.proyecto.interfaceService;

import java.util.List;
import java.util.Optional;

import com.example.proyecto.modelo.costo_proyecto;
import com.example.proyecto.modelo.info_proyecto;
public interface Icosto_proyectoService {

	
	public List<costo_proyecto>listarCosto_proyecto();
	public Optional<info_proyecto>listarId(int id);
	public int save (info_proyecto cp);
	public void delete (int id);
}
