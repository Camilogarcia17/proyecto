package com.example.proyecto.interfaceService;

import java.util.List;
import java.util.Optional;

import com.example.proyecto.modelo.info_proyecto;

public interface IproyectoService {

		public List<info_proyecto>listar();
		public Optional<info_proyecto>listarId(int id);
		public int save (info_proyecto i);
		public void delete (int id);
}
