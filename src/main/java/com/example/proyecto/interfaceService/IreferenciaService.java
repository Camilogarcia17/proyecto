package com.example.proyecto.interfaceService;

import java.util.List;
import java.util.Optional;

import com.example.proyecto.modelo.info_proyecto;
import com.example.proyecto.modelo.referencia;

public interface IreferenciaService {

	public List<referencia>listarReferencia();
	public Optional<info_proyecto>listarId(int id);
	public int save (info_proyecto r);
	public void delete (int id);
}
