package com.example.proyecto.interfaceService;

import java.util.List;
import java.util.Optional;

import com.example.proyecto.modelo.info_proyecto;
import com.example.proyecto.modelo.salario;

public interface IsalarioService {

	public List<salario>listar();
	public Optional<info_proyecto>listarId(int id);
	public int save (info_proyecto s);
	public void delete (int id);
}
