package com.example.proyecto.interfaceService;

import java.util.List;
import java.util.Optional;

import com.example.proyecto.modelo.info_proyecto;
import com.example.proyecto.modelo.usuario;

public interface IusuarioService {

	public List<usuario>listarUsuario();
	public Optional<info_proyecto>listarId(int id);
	public int guardar (info_proyecto s);
	public void eliminar (int id);
}
