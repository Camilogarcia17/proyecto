package com.example.proyecto.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.proyecto.modelo.costo_proyecto;

@Repository
public interface IcostoProyecto extends CrudRepository<costo_proyecto, Integer> {

}
