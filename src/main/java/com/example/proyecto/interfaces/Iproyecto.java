package com.example.proyecto.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.proyecto.modelo.info_proyecto;
@Repository	
public interface Iproyecto extends CrudRepository<info_proyecto, Integer> {

}
