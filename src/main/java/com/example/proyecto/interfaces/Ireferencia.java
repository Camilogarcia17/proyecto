package com.example.proyecto.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.proyecto.modelo.referencia;

@Repository
public interface Ireferencia extends CrudRepository<referencia, Integer>{

}
