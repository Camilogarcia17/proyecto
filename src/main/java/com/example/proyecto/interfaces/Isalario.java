package com.example.proyecto.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.proyecto.modelo.salario;

@Repository
public interface Isalario extends CrudRepository<salario, Integer> {

}
