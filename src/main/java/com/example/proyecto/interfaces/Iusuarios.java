package com.example.proyecto.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.proyecto.modelo.usuario;

@Repository
public interface Iusuarios extends CrudRepository<usuario, Integer> {

}
