package com.example.proyecto.modelo;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "costo")
public class costo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int  id_costo;
	private int  costo;
	private int  valor_cobrar;
	private int  valor_hora_cop;
	private int  valor_hora_usd;
	private int  cantidad;
	private int subtotal;
	private float dedicacion;
	private float hora_dedicacion;
	private float hora_esfuerzo;
	private int costo_total;
	private int total;
	private int total_USD;
	private int utilidad;
	
	@JoinColumn(name="id_salario", referencedColumnName = "id_salario")
	@ManyToOne(fetch = FetchType.EAGER)
	private salario salarios; 
	
	@JoinColumn(name="id_usuario", referencedColumnName = "id_usuario")
	@ManyToOne(fetch = FetchType.EAGER)
	private usuario usuarios;
	
	@JoinColumn(name="id_referencia", referencedColumnName = "id_referencia")
	@OneToOne
	private referencia referencias;
	
	
	public costo() {
		// TODO Auto-generated constructor stub
	}


	public int getId_costo() {
		return id_costo;
	}


	public void setId_costo(int id_costo) {
		this.id_costo = id_costo;
	}


	public int getCosto() {
		return costo;
	}


	public void setCosto(int costo) {
		this.costo = costo;
	}


	public int getValor_cobrar() {
		return valor_cobrar;
	}


	public void setValor_cobrar(int valor_cobrar) {
		this.valor_cobrar = valor_cobrar;
	}


	public int getValor_hora_cop() {
		return valor_hora_cop;
	}


	public void setValor_hora_cop(int valor_hora_cop) {
		this.valor_hora_cop = valor_hora_cop;
	}


	public int getValor_hora_usd() {
		return valor_hora_usd;
	}


	public void setValor_hora_usd(int valor_hora_usd) {
		this.valor_hora_usd = valor_hora_usd;
	}


	public int getCantidad() {
		return cantidad;
	}


	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	public int getSubtotal() {
		return subtotal;
	}


	public void setSubtotal(int subtotal) {
		this.subtotal = subtotal;
	}


	public float getDedicacion() {
		return dedicacion;
	}


	public void setDedicacion(float dedicacion) {
		this.dedicacion = dedicacion;
	}


	public float getHora_dedicacion() {
		return hora_dedicacion;
	}


	public void setHora_dedicacion(float hora_dedicacion) {
		this.hora_dedicacion = hora_dedicacion;
	}


	public float getHora_esfuerzo() {
		return hora_esfuerzo;
	}


	public void setHora_esfuerzo(float hora_esfuerzo) {
		this.hora_esfuerzo = hora_esfuerzo;
	}


	public int getCosto_total() {
		return costo_total;
	}


	public void setCosto_total(int costo_total) {
		this.costo_total = costo_total;
	}


	public int getTotal() {
		return total;
	}


	public void setTotal(int total) {
		this.total = total;
	}


	public int getTotal_USD() {
		return total_USD;
	}


	public void setTotal_USD(int total_USD) {
		this.total_USD = total_USD;
	}


	public int getUtilidad() {
		return utilidad;
	}


	public void setUtilidad(int utilidad) {
		this.utilidad = utilidad;
	}


	public salario getSalarios() {
		return salarios;
	}


	public void setSalarios(salario salarios) {
		this.salarios = salarios;
	}


	public usuario getUsuarios() {
		return usuarios;
	}


	public void setUsuarios(usuario usuarios) {
		this.usuarios = usuarios;
	}


	public referencia getReferencias() {
		return referencias;
	}


	public void setReferencias(referencia referencias) {
		this.referencias = referencias;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("costo [id_costo=");
		builder.append(id_costo);
		builder.append(", costo=");
		builder.append(costo);
		builder.append(", valor_cobrar=");
		builder.append(valor_cobrar);
		builder.append(", valor_hora_cop=");
		builder.append(valor_hora_cop);
		builder.append(", valor_hora_usd=");
		builder.append(valor_hora_usd);
		builder.append(", cantidad=");
		builder.append(cantidad);
		builder.append(", subtotal=");
		builder.append(subtotal);
		builder.append(", dedicacion=");
		builder.append(dedicacion);
		builder.append(", hora_dedicacion=");
		builder.append(hora_dedicacion);
		builder.append(", hora_esfuerzo=");
		builder.append(hora_esfuerzo);
		builder.append(", costo_total=");
		builder.append(costo_total);
		builder.append(", total=");
		builder.append(total);
		builder.append(", total_USD=");
		builder.append(total_USD);
		builder.append(", utilidad=");
		builder.append(utilidad);
		builder.append(", salarios=");
		builder.append(salarios);
		builder.append(", usuarios=");
		builder.append(usuarios);
		builder.append(", referencias=");
		builder.append(referencias);
		builder.append("]");
		return builder.toString();
	}
	
	
}
