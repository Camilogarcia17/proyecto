package com.example.proyecto.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="costo_proyecto")

public class costo_proyecto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private int id_costo_proyecto;
	private int sub_total_1;
	private int descuento;
	private int comision;
	private int sub_total_2;
	private int iva;
	private int total;
	
	@JoinColumn(name="id_costo")
	@OneToOne
	private costo costos; 
	
	public costo_proyecto() {
		// TODO Auto-generated constructor stub
	}

	public costo_proyecto(int id_costo_proyecto, int sub_total_1, int descuento, int comision, int sub_total_2, int iva,
			int total) {
		super();
		this.id_costo_proyecto = id_costo_proyecto;
		this.sub_total_1 = sub_total_1;
		this.descuento = descuento;
		this.comision = comision;
		this.sub_total_2 = sub_total_2;
		this.iva = iva;
		this.total = total;
	}

	public int getId_costo_proyecto() {
		return id_costo_proyecto;
	}

	public void setId_costo_proyecto(int id_costo_proyecto) {
		this.id_costo_proyecto = id_costo_proyecto;
	}

	public int getSub_total_1() {
		return sub_total_1;
	}

	public void setSub_total_1(int sub_total_1) {
		this.sub_total_1 = sub_total_1;
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}

	public int getComision() {
		return comision;
	}

	public void setComision(int comision) {
		this.comision = comision;
	}

	public int getSub_total_2() {
		return sub_total_2;
	}

	public void setSub_total_2(int sub_total_2) {
		this.sub_total_2 = sub_total_2;
	}

	public int getIva() {
		return iva;
	}

	public void setIva(int iva) {
		this.iva = iva;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	
}
