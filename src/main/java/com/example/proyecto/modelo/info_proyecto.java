package com.example.proyecto.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="info_proyecto")

public class info_proyecto {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)

	private int id_info_proyecto;
	private String fase;
	private String modulo;
	private String funcion;
	private float horas;
	private float totalHoras;
	private float semanas;
	private float totalSemanas;
	
	public info_proyecto() {
		// TODO Auto-generated constructor stub
	}

	public info_proyecto(int id_info_proyecto, String fase, String modulo, String funcion, float horas,
			float totalHoras, float semanas, float totalSemanas) {
		super();
		this.id_info_proyecto = id_info_proyecto;
		this.fase = fase;
		this.modulo = modulo;
		this.funcion = funcion;
		this.horas = horas;
		this.totalHoras = totalHoras;
		this.semanas = semanas;
		this.totalSemanas = totalSemanas;
	}

	public int getId_info_proyecto() {
		return id_info_proyecto;
	}

	public void setId_info_proyecto(int id_info_proyecto) {
		this.id_info_proyecto = id_info_proyecto;
	}

	public String getFase() {
		return fase;
	}

	public void setFase(String fase) {
		this.fase = fase;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getFuncion() {
		return funcion;
	}

	public void setFuncion(String funcion) {
		this.funcion = funcion;
	}

	public float getHoras() {
		return horas;
	}

	public void setHoras(float horas) {
		this.horas = horas;
	}

	public float getTotalHoras() {
		return totalHoras;
	}

	public void setTotalHoras(float totalHoras) {
		this.totalHoras = totalHoras;
	}

	public float getSemanas() {
		return semanas;
	}

	public void setSemanas(float semanas) {
		this.semanas = semanas;
	}

	public float getTotalSemanas() {
		return totalSemanas;
	}

	public void setTotalSemanas(float totalSemanas) {
		this.totalSemanas = totalSemanas;
	}
	

}
