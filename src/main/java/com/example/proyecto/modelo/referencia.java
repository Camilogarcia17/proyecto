package com.example.proyecto.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "referencia")
public class referencia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private int id_referencia;
	private int margen;
	private int carga;
	private int horasMes;
	private int dolar;
	private int duracionMes;
	private int duracionHora;
	private int nomina;
	@JoinColumn(name = "id_info_proyecto", unique = true)
	@OneToOne
	private info_proyecto info_proyecto;

	public referencia() {
		// TODO Auto-generated constructor stub
	}

	public referencia(int id_referencia, int margen, int carga, int horasMes, int dolar, int duracionMes,
			int duracionHora, int nomina, com.example.proyecto.modelo.info_proyecto info_proyecto) {
		super();
		this.id_referencia = id_referencia;
		this.margen = margen;
		this.carga = carga;
		this.horasMes = horasMes;
		this.dolar = dolar;
		this.duracionMes = duracionMes;
		this.duracionHora = duracionHora;
		this.nomina = nomina;
		this.info_proyecto = info_proyecto;
	}

	public int getId_referencia() {
		return id_referencia;
	}

	public void setId_referencia(int id_referencia) {
		this.id_referencia = id_referencia;
	}

	public int getMargen() {
		return margen;
	}

	public void setMargen(int margen) {
		this.margen = margen;
	}

	public int getCarga() {
		return carga;
	}

	public void setCarga(int carga) {
		this.carga = carga;
	}

	public int getHorasMes() {
		return horasMes;
	}

	public void setHorasMes(int horasMes) {
		this.horasMes = horasMes;
	}

	public int getDolar() {
		return dolar;
	}

	public void setDolar(int dolar) {
		this.dolar = dolar;
	}

	public int getDuracionMes() {
		return duracionMes;
	}

	public void setDuracionMes(int duracionMes) {
		this.duracionMes = duracionMes;
	}

	public int getDuracionHora() {
		return duracionHora;
	}

	public void setDuracionHora(int duracionHora) {
		this.duracionHora = duracionHora;
	}

	public int getNomina() {
		return nomina;
	}

	public void setNomina(int nomina) {
		this.nomina = nomina;
	}

	public info_proyecto getInfo_proyecto() {
		return info_proyecto;
	}

	public void setInfo_proyecto(info_proyecto info_proyecto) {
		this.info_proyecto = info_proyecto;
	}
	
	

}
