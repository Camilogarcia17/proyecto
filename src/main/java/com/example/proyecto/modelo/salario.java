package com.example.proyecto.modelo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name="salario")
public class salario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_salario;
	private int salario;
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "salarios")
	
	private List<costo> costos;
	
	public salario() {
		// TODO Auto-generated constructor stub
	}

	public int getId_salario() {
		return id_salario;
	}

	public void setId_salario(int id_salario) {
		this.id_salario = id_salario;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}

	public List<costo> getCostos() {
		return costos;
	}

	public void setCosto(List<costo> costos) {
		this.costos = costos;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("salario [id_salario=");
		builder.append(id_salario);
		builder.append(", salario=");
		builder.append(salario);
		builder.append("]");
		return builder.toString();
	}
	

	
}
