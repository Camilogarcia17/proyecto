package com.example.proyecto.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.proyecto.interfaceService.IcostoService;
import com.example.proyecto.interfaces.Icosto;
import com.example.proyecto.modelo.costo;



@Service
public class costoService implements IcostoService {

	
	@Autowired
	private Icosto data;
	
	@Override
	public List<costo> listarC() {
		return (List<costo> )data.findAll();
	}

	
	@Override
	public Optional<costo> listarId(int id) {
		return data.findById(id);
	}
	public int savee(costo c) {
		int resu =0;
		costo costo= data.save(c);
		if (!costo.equals(null)) {
			resu =1;
		}
		return 1;
	}
	
	@Override
	public void deletee(int id) {
		data.deleteById(id);
		
	}

}
