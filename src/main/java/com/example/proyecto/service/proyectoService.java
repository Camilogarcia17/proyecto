package com.example.proyecto.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.proyecto.interfaceService.IproyectoService;
import com.example.proyecto.interfaces.Iproyecto;
import com.example.proyecto.modelo.info_proyecto;

@Service
public class proyectoService  implements IproyectoService{

	@Autowired
	private Iproyecto data;
	
	@Override
	public List<info_proyecto> listar() {
		return (List<info_proyecto> )data.findAll();
	
	}

	@Override
	public Optional<info_proyecto> listarId(int id) {
		
		return data.findById(id);
	}

	@Override
	public int save(info_proyecto i) {
		int res =0;
		info_proyecto proyec= data.save(i);
		if (!proyec.equals(null)) {
			res =1;
		}
		return 1;
	}

	@Override
	public void delete(int id) {
		data.deleteById(id);
		
	}
	
	
	

}
